# JAZ

Find secrets hidden in commits

# Installing
```
~$ cargo install jaz
```

# Building
```
~$ cargo build --release
```

--------------------------------

# Usage
```
~$ jaz /path/to/repo
```
